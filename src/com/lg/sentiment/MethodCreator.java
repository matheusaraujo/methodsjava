package com.lg.sentiment;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * @author jpaulo
 * Design Patterns: Factory (Creator), Singleton
 */
public class MethodCreator {

	private static MethodCreator instance; //singleton
	public static String resourcePath; //singleton

	public synchronized static MethodCreator getInstance() {

		if (instance == null) {
			instance = new MethodCreator();
		}

		return instance;
	}

	public Method createMethod(int methodId) {

		URL resources = getClass().getClassLoader().getResource("lexicons");
		String lexDir = resources.getPath() + File.separator;


		resources = getClass().getClassLoader().getResource("models");
		String modDir = resources.getPath() + File.separator;

		switch (methodId) {
		case 1:
			return new Afinn("/lexicons/afinn/AFINN-111.txt");
		case 2:
			return new Emolex("/lexicons/emolex/NRC-emotion-lexicon-wordlevel-alphabetized-v0.92.txt");
		case 3:
			return new Emoticons("/lexicons/emoticons/positive.txt",
					"/lexicons/emoticons/negative.txt",
					"/lexicons/emoticons/neutral.txt");
		case 4:
			return new EmoticonDS("/lexicons/emoticonds/emoticon.words.advanced");
		case 5:
			return new HappinessIndex("/lexicons/happinessindex/anew.csv");
		case 6:
			return new MPQAAdapter(resourcePath + "/lexicons/mpqa_opinionfinder", resourcePath + "/models/mpqa_opinionfinder");
		case 7:
			return new NRCHashtagSentimentLexicon("/lexicons/nrchashtagsentiment/unigrams-pmilexicon.txt",
					"/lexicons/nrchashtagsentiment/bigrams-pmilexicon.txt",
					"/lexicons/nrchashtagsentiment/pairs-pmilexicon.txt");
		case 8:
			return new OpinionLexicon("/lexicons/opinionlexicon/positive-words.txt",
					"/lexicons/opinionlexicon/negative-words.txt");
		case 9:
			return new PanasT("/lexicons/panas/positive.txt",
					"/lexicons/panas/negative.txt",
					"/lexicons/panas/neutral.txt");
		case 10:
			return new Sann(resourcePath + "/models/StanfordTagger/english-bidirectional-distsim.tagger",
					"/lexicons/sann/emoticons.data",
					"/lexicons/sann/subjclust.tff",
					"/lexicons/sann/wordNetDictList.txt");
		case 11:
			return new Sasa("/lexicons/sasa/trainedset4LG.txt");
		case 12:
			return new SenticNet("/lexicons/senticnet/senticnet_v3_dataset.tsv");
		case 13:
			return new Sentiment140Lexicon("/lexicons/sentiment140/unigrams-pmilexicon.txt",
					"/lexicons/sentiment140/bigrams-pmilexicon.txt",
					"/lexicons/sentiment140/pairs-pmilexicon.txt");
		case 14:
			return new SentiStrengthAdapter(resourcePath + "/lexicons/sentistrength");
		case 15:
			return new SentiWordNet("/lexicons/sentiwordnet/SentiWordNet_3.0.0_20130122.txt",
					resourcePath + "/models/StanfordTagger/english-bidirectional-distsim.tagger");
		case 16:
			return new SoCal("/lexicons/socal/adj_dictionary1.11.txt",
					"/lexicons/socal/adv_dictionary1.11.txt",
					"/lexicons/socal/int_dictionary1.11.txt",
					"/lexicons/socal/noun_dictionary1.11.txt",
					"/lexicons/socal/verb_dictionary1.11.txt",
					"/lexicons/socal/google_dict.txt",
					resourcePath + "/models/StanfordTagger/english-bidirectional-distsim.tagger");
		case 17:
			return new StanfordAdapter();
		case 18:
			return new UmigonAdapter(resourcePath + "/lexicons/umigon");
		case 19:
			return new Vader("/lexicons/vader/vader_sentiment_lexicon.txt");
		default:
			return null;
		}
	}
}
