package com.lg.sentiment.util;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author johnnatan, matheus, elias, jpaulo
 */
public class Utils {

	/**
	 * common for some Methods
	 * regex w/o double backslashes: [!\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~]
	 */
	private static final String PUNCTUATION_REGEX = "[!\"#$%&'()\\*\\+,-\\./:;<=>?@\\[\\]\\^_`{|}~]";

	/**
	 * @param puncRegex regex containing all punctuation symbols
	 * @param text
	 * @return text with puctuation removed
	 */
	public static String removePunctuation(String puncRegex, String text) {

		return text.replaceAll(puncRegex, " ");
	}

	/**
	 * @param text
	 * @return text with puctuation symbols in PUNCTUATION_REGEX removed
	 */
	public static String removePunctuation(String text) {

		return removePunctuation(PUNCTUATION_REGEX, text);
	}

	/**
	 * @return whether str doesn't contain lowercase letter
	 */
	public static boolean isUpperString(String str) {

		return !str.matches("^.*[a-z].*$");

/*		for (int i=0; i<str.length(); ++i)
			if (Character.isLowerCase(str.charAt(i))) return false;
		return true; */		
	}

	/**
	 * @param list
	 * @param value
	 * @return how many elements <code>value</code> the <code>list</code> contains.
	 */
	public static int countOccurrences(List<String> list, String value) {

		int count = 0;
		for (String element : list) {

			if (element.equals(value)) {
				++count;
			}
		}

		return count;
	}

	/**
	 * @param text
	 * @param c
	 * @return how many times <code>c</code> occurs in <code>text</code>.
	 */
	public static int countChars(String text, char c) {

		int count = 0;
		for (int i=0; i < text.length(); ++i) {
			if (c == text.charAt(i)) {
				++count;
			}
		}
		
		return count;
	}

	/**
	 * @param d number to apply precision
	 * @param precision max size of decimal
	 * @return <code>d</code> with <code>precision</code> especified
	 */
	public static double setPrecision(double d, int precision) {

		BigDecimal bd = new BigDecimal(d).setScale(precision, RoundingMode.HALF_EVEN);
	    return bd.doubleValue();
	}

	/**
	 * 
	 * @param fileName
	 * @return Set containing all lexicon word read
	 */
	public static Set<String> readFileLinesToSet(final String fileName) {
		Set<String> set = new HashSet<>();
		try {
			InputStream in = Utils.class.getResourceAsStream(fileName);
			BufferedReader input = new BufferedReader(new InputStreamReader(in));
			String line;
			while((line = input.readLine()) != null) {
				line = line.trim();
				set.add(line);
			}
			input.close();
		} catch (IOException e) {
			System.err.println("Error opening file: " + fileName);
			System.exit(1989);
		}
		
		
		return set;
	}
	
	/**
	 * @param x
	 * @return log of x at base 2
	 */
	public static double log2(double x) {
		return Math.log(x) / Math.log(2);
	}
}
