package com.lg.sentiment;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jpaulo
 * sentence-level sentiment analysis method
 */
public abstract class Method {

	protected final int POSITIVE = 1;
	protected final int NEGATIVE = -1;
	protected final int NEUTRAL = 0;

	/**
	 * must load all lexicon needed to analyse sentences
	 */
	public abstract void loadDictionaries();

	/**
	 * @return the polarity
	 */
	public abstract int analyseText(String text);

	/**
	 * calls <code>analyseText</code> for each file's line and prints its polarity
	 */
	public ArrayList<Integer>  analyseFile(String filePath) {
		try {
			ArrayList<Integer> results = new ArrayList<>();
			BufferedReader br = new BufferedReader(new FileReader(filePath));

			String line = br.readLine();
			while (line != null) {
				Integer result;
				try{
					result = this.analyseText(line);
				} catch (OutOfMemoryError exception){
					result = 0;
				}

				results.add(result);
//				System.out.println(result);
				line = br.readLine();
			}
			br.close();
			return results;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
