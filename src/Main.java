import com.lg.sentiment.Method;
import com.lg.sentiment.MethodCreator;

/**
 * @author jpaulo
 * Design Patterns: Factory (Creator), Singleton
 */
public class Main {

	public static void main(String[] args) {
		String resourcePath = "";
		for(int i = 0; i < args.length; i++) {
			if(args[i].contains("--resources")){
				resourcePath = args[i+1];
			}
		}
		test02(resourcePath, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);
	}

	/**
	 * to be used (called from) as if was main()
	 * @param args
	 */
	private static void aux01(String[] args) {

		int argMethodId = 0;
		try {
			argMethodId = Integer.parseInt(args[3]);
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}

		MethodCreator creator = MethodCreator.getInstance();
		Method method = creator.createMethod(argMethodId);
		
		String argOption = args[0];
		String argTextOrFile = args[1];
		switch (argOption) {
		case "-t":
			int result = method.analyseText(argTextOrFile);
			System.out.println(result);
			break;
		case "-f":
			method.analyseFile(argTextOrFile);
			break;
		}
	}

	private static void test01(Method method) {
		System.out.println(method.analyseText("hate"));
		System.out.println(method.analyseText("republicano"));
		System.out.println(method.analyseText(".i love you!!.."));
		System.out.println(method.analyseText("i hate you"));
		System.out.println(method.analyseText("I LOVE YOU"));
		System.out.println(method.analyseText("i love you"));
		System.out.println(method.analyseText("i need you"));
		System.out.println(method.analyseText("i am fine"));
		System.out.println(method.analyseText("i understand you"));
		System.out.println(method.analyseText("hard strong happy LOVE"));
		System.out.println(method.analyseText("it hates and seems weep"));
		System.out.println(method.analyseText("test drive so good hard to see"));
		System.out.println(method.analyseText("i love and consider you so much"));
		System.out.println(method.analyseText("i love and understand you so much"));
		System.out.println(method.analyseText("The class was amazing BUT finished too late."));
		System.out.println(method.analyseText("i love you\nwhen the night has come and the land is dark"));
		System.out.println(method.analyseText("I good worse### worst&&& myself **happy# good bad fail forget test double $#%*"));
		System.out.println(method.analyseText("I happY** double times &&&good worse worst!!! myself happy good bad fail forget test double"));
		System.out.println(method.analyseText("i love you :) and consider!"));
		System.out.println(method.analyseText("my Website is http://ww2.test.com/index.html ... take a look!"));
		System.out.println(method.analyseText("This is AN emoticon :D see?"));
		System.out.println(method.analyseText("i DO LOVE you ): so... just consider these hashtags #TEAM_BR #TEAM-BR #TEAMBR and that my phone is +1 331 231-1290, my twitter is @nnnaaa_mmmeee and my website is http://www.website.test, ok!?"));
		System.out.println(method.analyseText(":):)(::):):):D:-):)8-D:)"));
		System.out.println(method.analyseText(":(:(:(:()::(:(:(:("));
		System.out.println(method.analyseText("AAAAAA! my phone is +1 771 355 1877"));
		System.out.println(method.analyseText("i NEED TAKE CARE with tags <b> and <i> in HTML5 to prosper."));

		//method.analyseFile("PATH/TO/FILE/WITH/sentences.txt");
	}


	public static void test02(String resourcePath, int ... methodsIds) {
		
		MethodCreator creator = MethodCreator.getInstance();
		creator.resourcePath = resourcePath;
		
		for (int id : methodsIds) {
			
			test01(creator.createMethod(id));
		}
	}
}