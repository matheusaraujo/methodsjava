/**
 * Created by matheus on 12/29/15.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import com.keysolutions.ddpclient.DDPClient;
import com.keysolutions.ddpclient.DDPListener;
import com.lg.sentiment.Method;
import com.lg.sentiment.MethodCreator;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static org.ejml.ops.EjmlUnitTests.assertEquals;

public class iFeel {
    public static Boolean checkLineDoneAndUpdate(LinkedTreeMap<String, Object> lineobj, String lineId, Map<String, Method> methods, DDPClient ddp){
        if(Boolean.parseBoolean(lineobj.get("done").toString())){
            System.out.println("Line: " + lineId + "Done");
            return true;
        }
        for (Map.Entry<String, Method> methodEntry : methods.entrySet()){
            if(lineobj.get(methodEntry.getKey()) == null){
                System.out.println("Line " + lineId + " not done due:" + methodEntry.getKey());
                return false;
            }
        }
        System.out.println("Line: " + lineId + " Done");
        Map<String,Object> options = new HashMap<String,Object>();
        Map<String,Object> setOptions = new HashMap<String,Object>();
        setOptions.put("done", true);
        options.put("$set", setOptions);
        ddp.collectionUpdate("linesCollection", lineId, options);

        return true;


    }
    public static Map convertObjectMap(Object obj){
        ArrayList<Object> list = new ArrayList<>();
        HashMap<String, String> map = null;
        try {
            map = (HashMap<String, String>) BeanUtils.describe(obj);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return map;
    }
    public static void generateMethods(Map<String, Method> methods, String resourcePath) {
        MethodCreator methodCreator = new MethodCreator();
        methodCreator.resourcePath = resourcePath;
        methods.put("afinn", methodCreator.createMethod(1));
        methods.put("emolex", methodCreator.createMethod(2));
        methods.put("emoticons", methodCreator.createMethod(3));
        methods.put("emoticonds", methodCreator.createMethod(4));
        methods.put("happinessindex", methodCreator.createMethod(5));
        methods.put("opinionfinder", methodCreator.createMethod(6));
        methods.put("nrchashtag", methodCreator.createMethod(7));
        methods.put("opinionlexicon", methodCreator.createMethod(8));
        methods.put("panas", methodCreator.createMethod(9));
        methods.put("sann", methodCreator.createMethod(10));
        methods.put("sasa", methodCreator.createMethod(11));
//        methods.put("senticnet", methodCreator.createMethod(12));
        methods.put("sentiment140", methodCreator.createMethod(13));
        methods.put("sentistrength", methodCreator.createMethod(14));
        methods.put("sentiwordnet", methodCreator.createMethod(15));
        methods.put("socal", methodCreator.createMethod(16));
        methods.put("stanford", methodCreator.createMethod(17));
        methods.put("umigon", methodCreator.createMethod(18));
        methods.put("vader", methodCreator.createMethod(19));

    }

    public static void localAnalyseMode(String resourcePath, String fileToAnalyse, String outputPath) {
        Map<String,Method> methods = new HashedMap<>();
        generateMethods(methods,resourcePath);



        System.out.println("Analysing file..." + fileToAnalyse);
        HashMap<String,ArrayList<Integer>> methods_results = new HashMap<>();
        for(String method_name : methods.keySet()){
            System.out.println(method_name + "...OK");
            ArrayList<Integer> results = methods.get(method_name).analyseFile(fileToAnalyse);
            methods_results.put(method_name,results);
        }

        try {
            PrintWriter writer = new PrintWriter(outputPath, "UTF-8");
            //Save CSV file
            //Print header
            String header = "line," + String.join("\t",methods_results.keySet());
            System.out.println(header);
            writer.println(header);
            //Print body
            BufferedReader br = new BufferedReader(new FileReader(fileToAnalyse));
            String line = br.readLine();
            int line_num = 0;
            while (line != null) {
                ArrayList<String> line_results = new ArrayList<>();
                line_results.add(line);
                for(String method_name : methods_results.keySet()){
                    Integer result = methods_results.get(method_name).get(line_num);
                    line_results.add(result.toString());
                }
                String csv_format = String.join("\t", line_results);
                System.out.println(csv_format);
                writer.println(csv_format);

                line = br.readLine();
                line_num++;
            }
            br.close();
            System.out.println(outputPath);
            System.out.println("CSV File: " + outputPath + " was created.");
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void testAnalyseMode(String resourcePath) {
        System.out.println("Test mode...");
        Main.test02(resourcePath, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);
    }
    public static void analyseLine(Map<String, Method> methods,LinkedTreeMap<String, Object> lineObj,Map<String,Object> options,Map<String,Object> setOptions, String lineId, DDPClient ddp){
        for (Map.Entry<String, Method> methodEntry : methods.entrySet()){
            String name = methodEntry.getKey();
            //Check if I already analysed it
            Method method = methodEntry.getValue();
            if(lineObj.get(name) != null){
                continue;
            }
            String text = lineObj.get("text").toString();
            try {
//                                    Thread t = new Thread() {
//                                        public void run() {

                Integer result =  0;
                try{
                    if(text.length() > 1 && text.trim().length() > 0){
//                                                    System.out.println("Text Length: " + text.length());
                        result = method.analyseText(text.substring(0, Math.min(text.length(), 250)));
//                        System.out.println(methodEntry.getKey() + ": " + result.toString() + " -> Text: " + text);
                    }
                }catch (java.lang.OutOfMemoryError e){
                    System.out.println("IFEEL WARNING OUT OF MEMORY FOR: " + text);
//                                                e.printStackTrace();
                    result = 0;
                }
                setOptions.put(name, result);
                options.put("$set", setOptions);

//                                        }
//                                    };
//                                    while (Thread.activeCount() >= 10){
//                                        Thread.sleep(1000);
//                                    }
//                                    t.start();
//                                    System.out.println("Running:" + Thread.activeCount());

            }
            catch (java.lang.IndexOutOfBoundsException e){
                //e.printStackTrace();
                System.out.println("Text: " + text);
                System.out.println("Unsupported Format, Setting Neutral");
                setOptions.put(name, 0);
                options.put("$set", setOptions);
            }


        }
        setOptions.put("done", true);
        options.put("$set", setOptions);
        ddp.collectionUpdate("linesCollection", lineId, options);
//        System.out.println(lineId + "->" + options.toString());
//        checkLineDoneAndUpdate(lineObj,lineId,methods,ddp);
        options.clear();
        setOptions.clear();
    }


    public static void iFeelAnalyseMode(String meteorIp, Integer meteorPort, Boolean orphan, String fileToAnalyse, String resourcePath) {
        System.out.println("iFeel mode...");
        System.out.printf("IP: " + meteorIp + "\n");
        System.out.printf("Port: " + meteorPort + "\n");
        System.out.printf("orphan: " + orphan + "\n");
        System.out.printf("fileToAnalyse: " + fileToAnalyse + "\n");

        try {
            final DDPClient ddp = new DDPClient(meteorIp, meteorPort);
            // create DDP client observer
            iFeelObserver iFeelOBS = new iFeelObserver();
            ddp.addObserver(iFeelOBS);

            // add observer
            ddp.connect();
            Object[] subscribeArgs = new Object[1];
            Map<String,Object> subscribeOptions = new LinkedHashMap<String,Object>();
            subscribeOptions.put("file", fileToAnalyse);
            subscribeArgs[0] = subscribeOptions ;
            Map<String,Method> methods = new LinkedHashMap<>();
            generateMethods(methods, resourcePath);


//          TODO: To remove
//           try{
//                if(orphan){
//                    ddp.subscribe("orphanLines", objects, iFeelOBS);
//                } else{
//                    ddp.subscribe("normalLines", objects, iFeelOBS);
//                }
//                ddp2.subscribe("linesAlmostDone", objects, iFeelOBS2);
//            }  catch (java.lang.NullPointerException e){
//                e.printStackTrace();
//            } catch (java.util.ConcurrentModificationException e){
//                e.printStackTrace();
//            }



//            String lineId;
            LinkedTreeMap<String, Object> lineObj;
            if(fileToAnalyse != null){
                ddp.subscribe("fileLines", subscribeArgs, iFeelOBS);
            } else if(orphan){
                ddp.subscribe("orphanLines", subscribeArgs, iFeelOBS);
            } else{
                ddp.subscribe("normalLines", subscribeArgs, iFeelOBS);
            }

            while (true) {
                try{
                    if(ddp.getState() == DDPClient.CONNSTATE.Closed){
                        System.out.printf("Closed, reconnect again\n");
                        ddp.deleteObserver(iFeelOBS);
                        iFeelOBS = new iFeelObserver();
                        ddp.addObserver(iFeelOBS);
                        ddp.connect();
                        Thread.sleep(1000);
                        if(fileToAnalyse != null){
                            ddp.subscribe("fileLines", subscribeArgs, iFeelOBS);
                        } else if(orphan){
                            ddp.subscribe("orphanLines", subscribeArgs, iFeelOBS);
                        } else{
                            ddp.subscribe("normalLines", subscribeArgs, iFeelOBS);
                        }
                        Thread.sleep(500);
                    }else{
                        Thread.sleep(500);
                    }

                    System.out.printf("State: " + ddp.getState().toString() + "\n");
                    System.out.printf("ErrorType: " +iFeelOBS.mErrorType + "\n");
                    System.out.printf("ErrorMsg: " +iFeelOBS.mErrorMsg + "\n");
                    System.out.printf("DdpState: " +iFeelOBS.mDdpState + "\n");

                    if(iFeelOBS.mErrorMsg != null && (iFeelOBS.mErrorMsg.contains("refused") || iFeelOBS.mErrorMsg.contains("error"))){
                        System.out.println("Refused or Error, force reconnect.");
                        ddp.disconnect();
                        Thread.sleep(500);
                    }



                    if (iFeelOBS.mDdpState == iFeelObserver.DDPSTATE.Connected && iFeelOBS.mReadySubscription != null && iFeelOBS.mCollections.containsKey("linesCollection") && iFeelOBS.mCollections.size() != 0) {
                        Set<HashMap.Entry<String, Object>> entriesSet = iFeelOBS.mCollections.get("linesCollection").entrySet();
                        //Converting to entriesSet to List
                        List<HashMap.Entry<String, Object>> entries = new ArrayList<HashMap.Entry<String, Object>>(entriesSet);
                        //Shuffle the list
                        Collections.shuffle(entries);
                        System.out.println("Total lines: " + entries.size());
                        //Define limit to get it again
                        int linesDone = 0;
                        int count = 0;
//                        while(linesDone < LIMIT_PER_LOOP){
                        entries = entries.size() > 50 ? entries.subList(0,50):entries;

                        for (Map.Entry<String, Object> entry : entries) {
                            count++;
                            //get line id
                            final String lineId = entry.getKey();
                            //get line obj (methods)
                            lineObj = (LinkedTreeMap<String, Object>) entry.getValue();
                            //Define results to send
                            Object[] methodArgs = new Object[1];
                            Map<String, Object> params = new HashMap<String, Object>();
                            params.put("id", lineId);

                            //if line doens't have a text or is done continue to the next
                            if(Boolean.parseBoolean(lineObj.get("done").toString())){
                                System.out.println("Already Done: " + lineId);
                                continue;
                            }

                            Map<String,Object> options = new HashMap<String,Object>();
                            Map<String,Object> setOptions = new HashMap<String,Object>();
                            //Start to Analyse Line
                            String stripped = StringUtils.strip((String)lineObj.get("text"));
                            if(lineObj.get("text") == null || lineObj.get("text").toString().contains("null") || stripped.equals("")){
                                System.out.println("Set: " + lineId + " DONE");
                                setOptions.put("done", true);
                                options.put("$set", setOptions);
                                ddp.collectionUpdate("linesCollection", lineId, options);
                                continue;
                            }
                            System.out.println(count + "/"+ entries.size() + " -> " + entry.getKey() + "/" + entry.getValue() + "\n");
                            analyseLine(methods,lineObj,options,setOptions,lineId, ddp);

//                            if(checkLineDoneAndUpdate(lineObj,lineId,methods,ddp)){
//                                System.out.println("Done!");
//                            }
                        }
//                        System.out.println("Checking lines...");
//                        Thread.sleep(500);
//                        for (Map.Entry<String, Object> entry : entries.subList(0,10)) {
//                            final String lineId = entry.getKey();
//                            lineObj = (LinkedTreeMap<String, Object>) entry.getValue();
//                            checkLineDoneAndUpdate(lineObj, lineId, methods, ddp);
//                        }
                    } else {
                        if (iFeelOBS.mCollections.size() == 0){
                            if(orphan){
                                System.out.printf("Empty orphanLines.\n");
                            } else {
                                System.out.printf("Empty normalLines.\n");
                            }

                        }
                    }
                } catch (InterruptedException e) {

                    e.printStackTrace();
                } catch (java.lang.NullPointerException e){
                    e.printStackTrace();
                } catch (java.util.ConcurrentModificationException e){
                    e.printStackTrace();
                }
            }


        } catch (java.net.URISyntaxException ex){
            System.out.printf("URI exception error.");
            ex.printStackTrace();
        }



//        Main.test02(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);

    }
    public static void main(String[] args) {
        String help_msg = "Little help to run the iFeel Jar\nThis is a Java implementation of a wide group of sentiment analysis techniques.\n" +
                "In this repository: https://bitbucket.org/matheusaraujo/sentimental-analysis-methods you can find the original implementation used at SentiBench-a benchmark comparison of state-of-the-practice sentiment analysis methods.\n" +
                "You probably want to analyse a file.\n" +
                "Use this command to analyse a file full of sentences, the output will be in .CSV format: java -jar iFeel.jar --resource 'resource/path' -f lines.txt.\n" +
                "JAR FILE PARAMETERS\n" +
                "--resources 'path' Resource folder path\n" +
                "--file or -f 'path' Path file to analyse\n" +
                "--output or -o 'path' Output file path.\n" +
                "--orphan Used restricted to iFeel Web Service\n" +
                "-p Used restricted to iFeel Web Service";


        String meteorIp = "localhost";
        String fileToAnalyse = null;
        Integer meteorPort = 3000;
        Boolean orphan = false;
        Map<String,Map<String,Boolean>> linesMethodDone = new HashedMap<>();
        Map<String,Boolean> lineMethodDoneFlags;
        Integer methodResult;
        String resourcePath = "./resources";
        String outputPath = "iFeelOutput.csv";
        Boolean fileMode = false;
        Boolean testMode = false;
        Boolean webMode = false;

        for(int i = 0; i < args.length; i++) {
            if(args[i].contains("-p")){
                meteorPort = Integer.parseInt(args[i+1]);
            }
            if(args[i].contains("--host")){
                meteorIp = args[i+1];
            }
            if(args[i].contains("--orphan")){
                orphan = true;
            }
            if(args[i].equals("-f") || args[i].contains("--file")){
                fileToAnalyse = args[i+1];
                fileMode = true;
            }
            if(args[i].equals("-o") || args[i].contains("--output")){
                outputPath = args[i+1];
            }
            if(args[i].equals("-w") || args[i].contains("--web")){
                webMode = true;
            }
            if(args[i].contains("--resources")){
                resourcePath = args[i+1];
            }
            if(args[i].contains("--test")){
                testMode = true;
            }
            if(args[i].contains("--help") || args[i].equals("-h")){
                System.out.println(help_msg);
                System.exit(0);
            }
        }

        if(fileMode){
            localAnalyseMode(resourcePath, fileToAnalyse,outputPath);
            System.exit(0);
        } else if (testMode){
            testAnalyseMode(resourcePath);
            System.exit(0);
        } else if (webMode){
            iFeelAnalyseMode(meteorIp, meteorPort, orphan, fileToAnalyse, resourcePath);
            System.exit(0);
        }
        System.out.println(help_msg);




    }
}
