/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package umigon4lg.Classifier;

import java.io.IOException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import umigon4lg.LanguageDetection.Cyzoku.util.LangDetectException;
import umigon4lg.Twitter.Tweet;

/**
 *
 * @author C. Levallois
 */
@Stateless
public class TweetLooper {

    List<Tweet> listTweets;
    @Inject ClassifierMachine cm;


    public TweetLooper() {
    }

    public List<Tweet> applyLevel1(List<Tweet> listTweets) throws LangDetectException, IOException {
        this.listTweets = listTweets;

        listTweets = cm.classify(listTweets);
        return listTweets;
    }
}
