package com.lg.sentiment;

import java.io.File;
import java.net.URL;

/**
 * @author jpaulo
 * Design Patterns: Factory (Creator), Singleton
 */
public class MethodCreator {

	private static MethodCreator instance; //singleton

	public synchronized static MethodCreator getInstance() {

		if (instance == null) {
			instance = new MethodCreator();
		}

		return instance;
	}

	public Method createMethod(int methodId) {

		//URL resources = getClass().getResource("/lexicons" + File.separator); //other way
		URL resources = getClass().getClassLoader().getResource("lexicons");
		String lexDir = resources.getPath() + File.separator;

		resources = getClass().getClassLoader().getResource("models");
		String modDir = resources.getPath() + File.separator;

		switch (methodId) {
		case 1:
			return new Afinn(lexDir + "afinn" + File.separator + "AFINN-111.txt");
		case 2:	
			return new Emolex(lexDir + "emolex" + File.separator + "NRC-emotion-lexicon-wordlevel-alphabetized-v0.92.txt");
		case 3:
			return new Emoticons(lexDir + "emoticons" + File.separator + "positive.txt", 
					lexDir + "emoticons" + File.separator + "negative.txt", 
					lexDir + "emoticons" + File.separator + "neutral.txt");
		case 4:
			return new EmoticonDS(lexDir + "emoticonds" + File.separator + "emoticon.words.advanced");
		case 5:
			return new HappinessIndex(lexDir + "happinessindex" + File.separator + "anew.csv");
		case 6:	
			return new MPQAAdapter(lexDir + File.separator + "mpqa_opinionfinder",
					modDir + File.separator + "mpqa_opinionfinder"); //TODO 2 folders - criar link de um pro outro?
		case 7:
			return new NRCHashtagSentimentLexicon(lexDir + "nrchashtagsentiment" + File.separator + "unigrams-pmilexicon.txt",
					lexDir + "nrchashtagsentiment" + File.separator + "bigrams-pmilexicon.txt", 
					lexDir + "nrchashtagsentiment" + File.separator + "pairs-pmilexicon.txt");
		case 8:
			return new OpinionLexicon(lexDir + "opinionlexicon" + File.separator + "positive-words.txt", 
					lexDir + "opinionlexicon" + File.separator + "negative-words.txt");
		case 9:
			return new PanasT(lexDir + "panas" + File.separator + "positive.txt", 
					lexDir + "panas" + File.separator + "negative.txt", 
					lexDir + "panas" + File.separator + "neutral.txt");
		case 10:
			return null;
//			return new Sann(modDir + "StanfordTagger" + File.separator + "english-bidirectional-distsim.tagger",
//                    lexDir + "sann" + File.separator + "emoticons.data",
//                    lexDir + "sann" + File.separator + "subjclust.tff",
//                    lexDir + "sann" + File.separator + "wordNetDictList.txt");
		case 11:
			return new Sasa(lexDir + "sasa" + File.separator + "trainedset4LG.txt");
		case 12:
			return new SenticNet(lexDir + "senticnet" + File.separator + "senticnet_v3_dataset.tsv");
		case 13:
			return new Sentiment140Lexicon(lexDir + "sentiment140" + File.separator + "unigrams-pmilexicon.txt",
					lexDir + "sentiment140" + File.separator + "bigrams-pmilexicon.txt", 
					lexDir + "sentiment140" + File.separator + "pairs-pmilexicon.txt");
		case 14:
			return new SentiStrengthAdapter(lexDir + "sentistrength");
		case 15:
			return null;
//			return new SentiWordNet(lexDir + "sentiwordnet" + File.separator + "SentiWordNet_3.0.0_20130122.txt",
//					modDir + "StanfordTagger" + File.separator + "english-bidirectional-distsim.tagger");
		case 16:
			return null;
//			return new SoCal(lexDir + "socal" + File.separator + "adj_dictionary1.11.txt",
//					lexDir + "socal" + File.separator + "adv_dictionary1.11.txt",
//					lexDir + "socal" + File.separator + "int_dictionary1.11.txt",
//					lexDir + "socal" + File.separator + "noun_dictionary1.11.txt",
//					lexDir + "socal" + File.separator + "verb_dictionary1.11.txt",
//					lexDir + "socal" + File.separator + "google_dict.txt",
//					modDir + "StanfordTagger" + File.separator + "english-bidirectional-distsim.tagger");
		case 17:
			return new StanfordAdapter();
		case 18:
			return new UmigonAdapter(lexDir + "umigon");
		case 19:
			return new Vader(lexDir + "vader" + File.separator + "vader_sentiment_lexicon.txt");
		default:
			return null;
		}
	}
}
