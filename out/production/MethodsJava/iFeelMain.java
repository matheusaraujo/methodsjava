import com.lg.sentiment.Method;
import com.lg.sentiment.MethodCreator;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * @author jpaulo
 * Design Patterns: Factory (Creator), Singleton
 */
public class iFeelMain {
	static ArrayList<Method> methods = new ArrayList<>();
	static PrintStream console;
	static PrintStream silenceprint;
	static HashMap<String,String> methodsDBNames = new HashMap<>();
	static Boolean testMode = true;
	static long startTime;
	static long stopTime;
    static String DBname = new String("ifeel2");
    static String DBuser = new String("matheus");
    static String DBpassword = new String("1234");
    static String DBhost = new String("localhost");


	public static void initializeiFeel() throws FileNotFoundException{

		//Silence by default
		System.setOut(silenceprint);
		System.setErr(silenceprint);

		//Instantiating methods
		int[] methods_ids = {1,2,3,4,5,7,8,9,12,13,14,18,19};
		MethodCreator creator = MethodCreator.getInstance();
		for(int id : methods_ids){
			methods.add(creator.createMethod(id));
		}

		//Fill methodsDBName
		methodsDBNames.put("Afinn","afinn");
		methodsDBNames.put("Emolex","emolex");
		methodsDBNames.put("Emoticons","emoticons");
		methodsDBNames.put("EmoticonDS","aniko");
		methodsDBNames.put("HappinessIndex","happiness");
		methodsDBNames.put("NRCHashtagSentimentLexicon","nrc");
		methodsDBNames.put("OpinionLexicon","opinionlexicon");
		methodsDBNames.put("PanasT","panas");
//		methodsDBNames.put("Sasa","sasa");
		methodsDBNames.put("SenticNet","scnet");
		methodsDBNames.put("Sentiment140Lexicon","sentiment140");
		methodsDBNames.put("SentiStrengthAdapter","sentistrength");
		methodsDBNames.put("SoCal","socal");
		methodsDBNames.put("Vader","vader");




	}

	public static void AnalyseLineDatabase(Integer lineID){
		try{
			//Connect to DB
			Connection conn = DriverManager.getConnection("jdbc:mysql://"+DBhost+"/" + DBname, DBuser, DBpassword);

			//Query lines to Analyse
			String query = "select * from line where id=" + lineID.toString() + ";";
			Statement statement = conn.createStatement();
			ResultSet dbresult = statement.executeQuery(query);
			if (dbresult.next()){
				Boolean isDone = dbresult.getString("done").equals("1");
				if(!isDone) {
					//Get line
					String text = dbresult.getString("text");
					System.out.println(text);

					//Run methods
					HashMap<String, Integer> methodsresults = runAllMethodsToText(text);

					//Save results
					for(String method : methodsresults.keySet()){
						if (methodsDBNames.get(method) == null) continue;
						String updatequery = "UPDATE line SET " + methodsDBNames.get(method) + " = ? WHERE id = ?";
						PreparedStatement updatestatement = conn.prepareStatement(updatequery);
						updatestatement.setFloat((int) 1, (float) methodsresults.get(method));
						updatestatement.setInt((int) 2, Integer.parseInt(dbresult.getString("id")));
						updatestatement.executeUpdate();
					}
				}
			}

		} catch (SQLException ex){ex.printStackTrace();}
	}

	public static void AnalyseFileDatabase(Integer fileID){
		try{
			//Connect to DB
			Connection conn = DriverManager.getConnection("jdbc:mysql://"+DBhost+"/" + DBname, DBuser, DBpassword);

			//Query lines to Analyse
			String query = "select * from line where file_owner=" + fileID.toString() + ";";
			//String query = "select * from line where file_owner=" + fileID.toString() + ";";
			Statement statement = conn.createStatement();
			ResultSet dbresult = statement.executeQuery(query);

			//Analyse lines from file
			while(dbresult.next()){
				Boolean isDone = dbresult.getString("done").equals("1");
				if(!isDone){

					//Get line
					String text = dbresult.getString("text");

					//Run methods
					HashMap<String, Integer> methodsresults = runAllMethodsToText(text);
					//Save results
					for(String method : methodsresults.keySet()){
						if (methodsDBNames.get(method) == null) continue;
						String updatequery = "UPDATE line SET " + methodsDBNames.get(method) + " = ? WHERE id = ?";
						PreparedStatement updatestatement = conn.prepareStatement(updatequery);
						updatestatement.setFloat((int) 1, (float) methodsresults.get(method));
						updatestatement.setInt((int) 2, Integer.parseInt(dbresult.getString("id")));

						//Verbose
						System.out.println("MethodName: " + methodsDBNames.get(method) + "\t" + methodsresults.get(method));
						System.out.println(updatestatement.toString());
						updatestatement.executeUpdate();
						System.setOut(silenceprint);


					}
				}


			}
		} catch (SQLException ex){ex.printStackTrace();}
	}

	public static void main(String[] args) throws FileNotFoundException{
        console = System.out;
        silenceprint = new PrintStream(new FileOutputStream("/dev/null"));
		silenceprint = System.out;

        startTime = System.currentTimeMillis();
		initializeiFeel();
		System.setOut(console);

        if(args[0].equals("--test")){
            testMode = true;
            System.out.println("TEST MODE");

            System.setOut(console);
            System.setOut(silenceprint);


                System.setOut(console);
                try{
                    //Connect to DB

                    Connection conn = DriverManager.getConnection("jdbc:mysql://"+DBhost+"/" + DBname, DBuser, DBpassword);
                    String query = "select * from line";
                    Statement statement = conn.createStatement();
                    ResultSet dbresult = statement.executeQuery(query);
                    System.out.println("Database Connection OK");

                } catch (SQLException ex){ex.printStackTrace();}
                System.setOut(silenceprint);

                System.setOut(console);
                System.setErr(console);
                stopTime = System.currentTimeMillis();

                System.out.println("Tests: \t Loading Time:" + Long.toString(stopTime - startTime));
                System.out.println("I love you. :)! Tests:");
                for(Method m : methods){
                    startTime = System.currentTimeMillis();
                    System.out.println(m.getClass().getSimpleName() + ": " + m.analyseText("I love you. :)"));
                    stopTime = System.currentTimeMillis();
                    System.out.println("Time: " + Long.toString((stopTime - startTime)));

                }
                System.out.println("dASR5$$TGFVXZDFW$5ŜDFGsdaf asdfa sdfFGVSARE!@$Rxzckljsjkdlkajlkljer3q2io431289742398057asdf4890ts fDFADFQ%23452df asDE!@$%$%TYUHVNCXBVSZDTR$$%23wtg sgfsdW#$% xcv Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra, nibh sit amet aliquet blandit, tortor eros tincidunt magna, eget vestibulum diam orci sit amet lectus. Phasellus ultricies arcu risus, eu suscipit ligula vestibulum vitae. Sed et quam quis elit egestas commodo eu at risus. Nullam ut cursus mi, sed tincidunt nisi. Aenean vulputate finibus elit. Aliquam a tellus sit amet lectus maximus elementum. Pellentesque bibendum lectus mauris, in condimentum erat sollicitudin vel. In id ornare lectus. Fusce magna urna, feugiat vitae purus suscipit, interdum iaculis mi. Nulla facilisi. Mauris rhoncus, mi vel imperdiet accumsan, felis velit mollis eros, sit amet pellentesque nisl mi a mauris. Donec dignissim ullamcorper lacinia. Cras eu erat id dolor pharetra finibus. Nullam malesuada nunc vitae mollis dignissim. Mauris lacinia nisl vel elit pulvinar iaculis. Aliquam est metus, consequat nec enim eu, tristique rutrum nibh. ");
                for(Method m : methods){
                    startTime = System.currentTimeMillis();
                    System.out.println(m.getClass().getSimpleName() + ": " + m.analyseText("I love you. :)"));
                    stopTime = System.currentTimeMillis();
                    System.out.println("Time: " + Long.toString((stopTime - startTime)));

                }
                System.setOut(silenceprint);
                System.setErr(silenceprint);

            }


        else if (args[0].equals("--line")){
			//Run iFeel text mode: Read text from stdin, analise it, return a JSON with results
			int lineID = Integer.parseInt(args[1]);
		} else if (args[0].equals("--file")){
			//Run iFeel db mode: Read a id from a file at bd and analyse it.
			int fileID = Integer.parseInt(args[1]);
			AnalyseFileDatabase(fileID);
		}
        if(args.length== 0){
            System.out.println("No mode.");
			try {
				listenerToiFeel();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * used by ifeel
	 */
	private static void listenerToiFeel() throws IOException{
		//Running methods to stdin text
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		HashMap<String, Integer> results;
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
			if(line.equals("q")){
				break;
			}
			else{
				results = runAllMethodsToText(line);
//				printJSONresults(results);
			}
		}
		reader.close();
	}

	/**
	 * Run all methods to specific text and print a JSON
	 */
	private static HashMap<String, Integer> runAllMethodsToText(String text){

		HashMap<String, Integer> results = new HashMap<>();
		for(Method m: methods){
			if(m != null){
				int result = m.analyseText(text);
				String name = m.getClass().getSimpleName();
				results.put(name, result);
			}
		}
		return results;
	}

//	private static void printJSONresults(HashMap<String,Integer> results){
//		JSONObject resultjson = new JSONObject(results);
//		System.setOut(console);
//		System.out.println(resultjson.toString());
//		System.setOut(silenceprint);
//	}

}

