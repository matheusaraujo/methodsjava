iFeel Java Methods
==================
This is an easy to run iFeel methods.

Run
=====
cd iFeel
java -jar iFeel.jar --resources ../resources/ --file example.txt --output out.csv


Original Method Implementation
===============================
You can download the original implementation at: https://bitbucket.org/matheusaraujo/sentimental-analysis-methods

MORE INFORMATION ABOUT OUR WORK ON SENTIMENT ANALYSIS:
======================================================

You can download the code of all the sentiment analysis we implemented on iFeel 2.0 at: https://bitbucket.org/matheusaraujo/methodsjava
You can download the code of all the sentiment analysis we implemented at SentiBench-a benchmark comparison of state-of-the-practice sentiment analysis methods at: https://bitbucket.org/matheusaraujo/sentimental-analysis-methods
Here is the link to the system: www.ifeel.dcc.ufmg.br 

Please, check the README file for instructions to install and run those methods.
A description of how our iFeel system works can be found in two publications:
Ifeel 1.0 and ifeel 2.0. 
http://homepages.dcc.ufmg.br/~fabricio/download/de03-araujo.pdf
http://www.dcc.ufmg.br/~fabricio/download/icwsm2016-ifeel2.pdf

Our first effort to compare part of these methods appeared at COSN’13. 
http://homepages.dcc.ufmg.br/~fabricio/download/cosn127-goncalves.pdf

We have been working on a journal extension in which we considered 18 datasets to compare all those methods. The description of all the methods implemented on iFeel are better detailed in this journal version. 
The paper has just been accepted for publication and will soon appear on EPJ Data Science. This paper can be found at: 
http://www.epjdatascience.com/content/5/1/23

You can download these 18 English labeled datasets at:
 https://bitbucket.org/matheusaraujo/ifeel-benchmarking-datasets

We also have explored sentiment analysis in multiple languages. To that end, we are also sharing labeled datasets in Portuguese, Arabic, Dutch, German, French, Italian, Portuguese, Russian, Spanish, and Turkish.  The paper that describes this effort appeared in SAC’16 and can be found here. 
http://homepages.dcc.ufmg.br/~fabricio/sentiment-languages-dataset/index.htm
http://homepages.dcc.ufmg.br/~fabricio/download/sac2016-translation.pdf

We are also sharing a resource of News headlines, along with their measured sentiments, popularity, category, etc. You can download the data used in ICWSM'15 paper at. 
http://homepages.dcc.ufmg.br/~fabricio/database_news.htm
http://homepages.dcc.ufmg.br/~fabricio/database_news.txt
http://homepages.dcc.ufmg.br/~fabricio/database_news.csv

Finally, if you aim at exploring supervised approaches, you may find interesting to incorporate the meta-level features we describe in our WSDM’16 paper. 
http://www.hompepages.dcc.ufmg.br/~fabricio/download/wsdm377-canutoA1.pdf

